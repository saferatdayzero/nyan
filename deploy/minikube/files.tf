# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "local_sensitive_file" "values_yaml" {
  content = yamlencode({
    ingress = {
      annotations = {
        "cert-manager.io/cluster-issuer" = "selfsigned"
      }
      className = "nginx"
      enabled   = true
      hosts = [{
        host = local.lens_hostname
        paths = [{
          path     = "/"
          pathType = "ImplementationSpecific"
        }]
      }]
      tls = [{
        hosts      = [local.lens_hostname]
        secretName = "${local.lens_hostname}-tls" # pragma: allowlist secret
      }]
    }
    settings = {
      allowedHosts    = [local.lens_hostname]
      databaseUrl     = local.lens_postgresql_url
      secretKey       = random_password.lens_secret_key.result
      cacheUrl        = local.lens_valkey_url
      brokerUrl       = local.lens_valkey_url
      ssl             = true
      siteEnvironment = "minikube"
    }
  })
  filename        = "${path.module}/values.yaml"
  file_permission = "0600"
}

resource "local_sensitive_file" "settings_ini" {
  content         = <<-EOF
[settings]
LENS_SECRET_KEY = ${random_password.lens_secret_key.result}
LENS_DATABASE_URL = postgres://lens:${local.lens_postgresql.password}@localhost:5432/lens
LENS_CACHE_URL = redis://:${local.lens_valkey.password}@localhost:6379/0
LENS_BROKER_URL = redis://:${local.lens_valkey.password}@localhost:6379/0
LENS_SITE_ENVIRONMENT = local
EOF
  filename        = "${path.module}/settings.ini"
  file_permission = "0600"
}
