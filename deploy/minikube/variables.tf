# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

variable "config_path" {
  type    = string
  default = "~/.kube/config"
}

variable "config_context" {
  type    = string
  default = "minikube"
}
