# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.11.0"
  constraints = "~> 2.11.0"
  hashes = [
    "h1:ohKpsfAdRWWQZpqbK99sol2Pbp7betwUKUSZksVjI3s=",
    "zh:2b505116e91325a26a0749ad9820ec4f2a9f47e195a84d6fd1f41d86b92bdefd",
    "zh:2f44100e7e6c1063a3c872066a04378b7336d5b4fbc1f6581d3e69f85dbbba3f",
    "zh:71fec2cda63d13250cf460e48e8a1e8f3200f93f15e5ed02df871fe86d6c559c",
    "zh:7a2b5c182ba8bb980a44642f475334eca8e5e689083003471d0f8272482bb209",
    "zh:9cddaf003b66530a02b2efb574ef9fecaad5a8f00ea744e435de5c6672a14a22",
    "zh:ac6cb892a2eeddae58548c9c252e266a07df7f8b894670b68729c7eae562ac92",
    "zh:ad88f8d7390ce32204b01cd74133c7ed85fbc5dab2add6c6784ce52be7a99cae",
    "zh:cc7e007ea23739ebe4efb75bf2fae2c95aa18c63ea109357d7f8f79940182f61",
    "zh:da97c83e849c74da08bb1ed70edd062db1b2469fe48fb7d44399baecba8c1009",
    "zh:f94ce933127a56d3f6019e483a1f6f5edb2255e5edfed99ecfac26e444995152",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.23.0"
  constraints = "~> 2.23.0"
  hashes = [
    "h1:vAfwUaEN5WcOXQAgH2103jNeccS+esOXDw+gGJdxbcM=",
    "zh:0493758b92d54ab8634540e2e2417fe5de956f2756e8de20f369be22bf98fb26",
    "zh:177b271f1cf02f4bbd7bed7c6a846898f66dda4751e8228017c38765f6a0f00c",
    "zh:1ce3834baf2667b1e6ba941324756a8a374e0e1eaabb5e9e204a79e8c429c228",
    "zh:347af45ce6260886ac642121201709249b53e890b3699baa418fdc9c1cbbd33a",
    "zh:4eaf5f965c4c1ea0eddfde6ca718400def8c8d9c517d4689520a776f54e48511",
    "zh:541b89dbe56f07707e44bfedc36a9e5471b6ddca84f90e493dc18e2101a2e363",
    "zh:59b32de1d4bf7fb31a00675bf51e7d98e8243e5cf7e0e84c49768c70c7f70631",
    "zh:8948caac6323b3f379943671c5b9dd77749de68fed6d67fbac79c1f5b9dea7bf",
    "zh:9cb44cfc0448a62a3e473cd5d230d3a66ff0cd1bc37047b333740649a9797e43",
    "zh:c14f7bb31a31cf600678ab80885cfb263068ec8db5a2fdded082c7e05c6bff5c",
  ]
}

provider "registry.opentofu.org/hashicorp/local" {
  version     = "2.4.1"
  constraints = "~> 2.4.0"
  hashes = [
    "h1:ZmiQvqAmblLC08P/NR+AVQtZrD+GGRpjFWcl+qX3+tE=",
    "zh:0d4d7a980ba3479459ab8a231019588d38615b22b97600419c3c7482ee82bc8b",
    "zh:16cf003a5bff1c5cae32e29af30f54376aec29b21ce26768d5fa59cc8b3bac3e",
    "zh:469539918519547b2b87a9301ad811e59e50d4f217c36da01e0d981b45a393e9",
    "zh:620234f4c7dcfd00fa4b7d15e71fa055ada4a8afe084f84a40d06d02fc7e7c08",
    "zh:68fd2f982c5fecff972e03eced690077911083f782a237d5a026597ab387f737",
    "zh:7bc30de9089a3d060f9866e08f6bd766351880026a458a4b7443cb110d47dc16",
    "zh:ad772631dfa005fdd6f7cc2ce33d1771218574b5153c5d601d7584fe9e8a0da2",
    "zh:cb6f16481607a3899e1d0a8f9fcacf246fb4927ee274da69c7d1e75fc4960713",
    "zh:dcac954e9e401c53db3750ab3df22173f7ba288e978da348f97ddbbc4a33abb1",
    "zh:eeeb0583e152403bdb473de5f133b48cef04af8f7c3f38ee7dc6d034df91534d",
  ]
}

provider "registry.opentofu.org/hashicorp/null" {
  version     = "3.2.3"
  constraints = "~> 3.2.3"
  hashes = [
    "h1:LF8arSzHfhbyQSFtTMTYEqCM34klzrbAQBJMHYCs9d8=",
    "zh:1d57d25084effd3fdfd902eca00020b34b1fb020253b84d7dd471301606015ac",
    "zh:65b7f9799b88464d9c2ec529713b7f52ea744275b61a8dc86cdedab1b2dcb933",
    "zh:80d3e9c95b7b4ae7c54005cd127cae82e5c53d2b7023ef24c147337bac9dadd9",
    "zh:841b60c07683e4bf456799ccd718896fdafdcc2c49252ae09967f2e74d8c8a03",
    "zh:8fa1c592a9c78222e35713c6edb3f1f818a4c6f3524a30a209f0a7e919827b68",
    "zh:bb795cc1429e09466840c09d39a28edf1db5070b1ec76822fc1173906a264572",
    "zh:da1784818a89bea29dfe660632f0060a7a843e4e564d74435fbeca002b0f7d2a",
    "zh:f409bf21b1cdaa6dac47cd79806f3d93f67e9507fe4dbf33b0165335f53bc2e1",
    "zh:fbea7a1ff84b430ba9594698e93196d81d03e4036de3d1cafccb2a96d5b38581",
    "zh:fbf0c84663a7e85881388d7d71ac862184f05fbf2d17ecf76bc5d3d7503ea260",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.5.1"
  constraints = "~> 3.5.1"
  hashes = [
    "h1:tW+G7lgqbHUtraKHPWuotYHlME1vcAf50YvOeHQlGHg=",
    "zh:0002dd4c79453da5bf1bb9c52172a25d042a571f6df131b7c9ced3d1f8f3eb44",
    "zh:49b0f8c2bd5632799aa6113e0e46acaa7d008f927665a41a1f8e8559fe6d8165",
    "zh:56df70fca236caa06d0e636c41ab71dd1ced05375f4ddcb905b0ed2105737048",
    "zh:58e4de40540c86b9e2e2595dac1318ba057718961a467fa9727866f747693eb2",
    "zh:5992f11c738812ccd7476d4c607cb8b76dea5aa612be491150c89957ec395ddd",
    "zh:7ff4f0b7707b51737f684e96d85a47f0dd8be0f72a3c27b0798755d3faad15e2",
    "zh:8e4b0972e216c9773ab525accfa36eb27c44c751b06b125ecc53f4226c91cea8",
    "zh:d8956cc5abcd5d1173b6cc25d5d8ed2c5cc456edab2fddb774a17d45e84820cb",
    "zh:df7f9eb93a832e66bc20cc41c57d38954f87671ec60be09fa866273adb8d9353",
    "zh:eb583d8f03b11f0b6c535375d8ed0d29e5f7f537b5c78943856d2e8ce76482d9",
  ]
}
