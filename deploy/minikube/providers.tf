# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

provider "kubernetes" {
  config_path    = var.config_path
  config_context = var.config_context
}

provider "helm" {
  kubernetes {
    config_path    = var.config_path
    config_context = var.config_context
  }
}
