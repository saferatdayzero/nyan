# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  selfsigned_clusterissuer_manifest_path = "${path.module}/templates/selfsigned-clusterissuer.yaml"
}

resource "kubernetes_namespace_v1" "cert_manager" {
  metadata {
    name = "cert-manager"
  }
}

resource "helm_release" "cert_manager" {
  name       = "cert-manager"
  namespace  = kubernetes_namespace_v1.cert_manager.metadata.0.name
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "1.16.1"
  timeout    = 120

  set {
    name  = "crds.enabled"
    value = true
  }
}

resource "null_resource" "selfsigned_clusterissuer" {
  depends_on = [helm_release.cert_manager]

  provisioner "local-exec" {
    command = "kubectl apply -f \"${local.selfsigned_clusterissuer_manifest_path}\""
  }

  triggers = {
    manifest = sha256(file(local.selfsigned_clusterissuer_manifest_path))
  }
}
