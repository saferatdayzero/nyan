# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  lens_hostname = "lens.local"

  lens_postgresql = {
    username      = "lens"
    database      = "lens"
    password      = random_password.lens_postgresql.result
    root_password = random_password.lens_postgresql_root.result
    hostname      = "lens-postgresql.lens.svc.cluster.local"
    port          = 5432
  }

  lens_postgresql_url      = "postgres://${local.lens_postgresql.username}:${local.lens_postgresql.password}@${local.lens_postgresql.hostname}:${local.lens_postgresql.port}/${local.lens_postgresql.database}"
  lens_postgresql_root_url = "postgres://postgres:${local.lens_postgresql.root_password}@${local.lens_postgresql.hostname}:${local.lens_postgresql.port}/${local.lens_postgresql.database}"

  lens_valkey = {
    password = random_password.lens_valkey.result
    hostname = "lens-valkey-master.lens.svc.cluster.local"
    database = "0"
    port     = 6379
  }

  lens_valkey_url = "redis://:${local.lens_valkey.password}@${local.lens_valkey.hostname}:${local.lens_valkey.port}/${local.lens_valkey.database}"
}
