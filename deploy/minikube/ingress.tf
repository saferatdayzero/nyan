# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "ingress" {
  metadata {
    name = "ingress"
  }
}

resource "helm_release" "ingress" {
  name       = "ingress"
  namespace  = kubernetes_namespace.ingress.metadata.0.name
  repository = "oci://registry-1.docker.io/bitnamicharts"
  chart      = "nginx-ingress-controller"
  version    = "11.4.0"

  values = [yamlencode({
    publishService = {
      enabled = true
    }
  })]
}
