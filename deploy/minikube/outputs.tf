# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "lens_postgresql_credentials" {
  value = {
    postgresql = merge(
      local.lens_postgresql,
      {
        url      = local.lens_postgresql_url
        root_url = local.lens_postgresql_root_url
      },
    )
  }
  sensitive = true
}

output "lens_valkey_credentials" {
  value = {
    valkey = merge(
      local.lens_valkey,
      {
        url = local.lens_valkey_url
      },
    )
  }
  sensitive = true
}
