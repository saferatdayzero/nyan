# Database

## Squash Migrations

Follow the recommendations of the [Django docs for squashing migrations](https://docs.djangoproject.com/en/5.1/topics/migrations/#squashing-migrations).

- pull the latest changes
- run `makemigrations` to check for any needed migrations, and migrate them
- run `squashmigrations` for each app that needs it
- Some tests run by `manage.py test` will test the migrations against an empty database
- commit the squashed migrations files
- have other developers migrate
- create a release, update staging, then update production, you can double check
  migrations have happened in staging and production by doing exec into the
  lens-app and running ./manage.py dbshell then running then querying for
  migrations and looking for the squashed migrations.
- after all versions of the app have been migrated, transition the squashed migration to a normal migration.
- delete all the migration files replaced by the squashed migrations
- update all migrations that depend on the deleted migrations to depend on the squashed migration
- remove the replaces attribute in the Migration classes of the squashed migration files
- commit, push, and release the changes
