# Field reference

## Categories

Categories are a means to organize Prompts into buckets. All Prompts must be
organized into Categories in order to appear in the web views.

### title

The display name for the category.

### brief

A short description of what the category is for. It is shown to the user.

### description

A long-form description of the category. Markdown is supported.

## Prompts

Prompts present a falsifiable viewpoint, observation, or question to be answered
by Responses.

### key

A unique, human-readable identifier assigned to the Prompt. **Pick a good one
and avoid changing it**. Eventually, this key will be locked down and used by
automation to retrieve Prompts. It must be
[kebab-case](https://developer.mozilla.org/en-US/docs/Glossary/Kebab_case), but
forward slashes (`/`) are also allowed. This is used in the admin interface but
isn't shown in the report.

### title

A free-form short question or statement to be evaluated.

### category

Controls to which Category this Prompt is assigned.

### description

Long-form description of a Prompt. Markdown supported.

## Responses

### prompt

Associates this Response with a Prompt.

### key

A human-readable identifier assigned to the Response. **Pick a good one and
avoid changing it**. Eventually, this key will be locked down and used by
automation to retrieve Responses. It must be
[kebab-case](https://developer.mozilla.org/en-US/docs/Glossary/Kebab_case). Keys
are unique to the Prompt the Response is assigned to.

### comparable

Controls whether a Response can be meaningfully compared with other "comparable"
Responses attached to the same Prompt. **Responses must be comparable for their
Findings to be shown in the site comparison view**.

### cast

Cast the value of the Finding (discussed later) to the specified type. There are
a fixed number of types to choose from.

### message

Message printed to the user in a safety report. It is intended to be short,
declarative, and actionable. It should stand on its own and not require
additional context to interpret:

```
This site does not use SSL.
```

Use `{value}` in the message string if you want the value associated with a
Finding to show in the message:

```
This domain is {value} years(s) old.
```

### message_singular

If specified, used when a value is an `integer` and there is exactly one item:

```
This domain is one year old.
```

### shelf_life

Controls how long a Finding is valid after creation before it should be
considered expired. If no value is provided, Findings do not expire.

### description

Long-form description of a Response. Markdown supported.

## Resources

A Resource represents any resource that can be identified via a [Uniform
Resource
Identifier](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier#syntax),
or URI.

### uri

The URI of the resource to be evaluated.

### tags

Tags associated with this Resource.

### related

Resources that share Findings with this Resource. For example: `dns:google.com`
and `https://google.com`. This support is not yet implemented.

## Tags

Tags help to categorize similar Resources.

### key

A human-readable identifier assigned to the Response. **Pick a good one and
avoid changing it**. Eventually, this key will be locked down and used by
automation to retrieve Responses. It must be
[kebab-case](https://developer.mozilla.org/en-US/docs/Glossary/Kebab_case). Keys
are unique to the Prompt the Response is assigned to.

### title

User-visible name of the tag. Spaces and special characters are allowed.

### description

Long-form description of a Tag. Markdown supported.

## Sources

### key

A human-readable identifier assigned to the Response. **Pick a good one and
avoid changing it**. Eventually, this key will be locked down and used by
automation to retrieve Responses. It must be
[kebab-case](https://developer.mozilla.org/en-US/docs/Glossary/Kebab_case). Keys
are unique to the Prompt the Response is assigned to.

### name

The full name of the source of the information. It will appear in reports if
[short_name](#short_name) is not defined.

### short_name

Optional shorter version of the name that will appear in reports.

### url

The home page for the information source.

### brief

Short description of a Tag.

### description

Long-form description of a Tag. Markdown supported.

## Findings

### resource

The Resource the Finding is associated with.

### response

The Response the Finding is associated with.

### source

Where the information for the Finding came from.

### value

The optional value from the Source. Sometimes the Response message does not need
a value.
