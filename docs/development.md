# Development

You will likely want to keep open multiple terminal windows while working on
lens.

## Install requirements

The following commands are required:

- [helm](https://helm.sh/docs/intro/quickstart/)

- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

- [minikube](https://minikube.sigs.k8s.io/docs/start/)

- [skaffold](https://skaffold.dev/docs/install/)

- [tofu](https://opentofu.org/docs/intro/install/)

- [uv](https://github.com/astral-sh/uv)

## Create Minikube cluster

A Minikube deployment of dsri lens can be found in the
[`deploy/minikube`](../deploy/minikube) directory.

First, create a Minikube cluster, setting the CPU and memory limits to values
appropriate for your system (the default is 2 CPUs and 4GB memory):

!!! note

    `--cni=cilium` allows NetworkPolicy resources to be used in Minikube.

```bash
minikube start --cni=cilium --cpus 4 --memory 8GB
```

Wait for the cluster to be created:

```console
$ minikube start --cpus 4 --memory 8GB
😄  minikube v1.33.1 on Debian bookworm/sid
✨  Automatically selected the docker driver. Other choices: virtualbox, ssh
📌  Using Docker driver with root privileges
👍  Starting "minikube" primary control-plane node in "minikube" cluster
🚜  Pulling base image v0.0.44 ...
🔥  Creating docker container (CPUs=4, Memory=8192MB) ...
🐳  Preparing Kubernetes v1.30.0 on Docker 26.1.1 ...
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔗  Configuring bridge CNI (Container Networking Interface) ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

Verify that the cluster is fully initialized by ensuring that all cluster pods
are running:

```bash
kubectl get pod -A
```

```console
$ kubectl get pod -A
NAMESPACE     NAME                               READY   STATUS    RESTARTS       AGE
kube-system   coredns-7db6d8ff4d-ftngl           1/1     Running   0              2m27s
kube-system   etcd-minikube                      1/1     Running   0              2m41s
kube-system   kube-apiserver-minikube            1/1     Running   0              2m41s
kube-system   kube-controller-manager-minikube   1/1     Running   0              2m43s
kube-system   kube-proxy-mh92p                   1/1     Running   0              2m28s
kube-system   kube-scheduler-minikube            1/1     Running   0              2m41s
kube-system   storage-provisioner                1/1     Running   1 (117s ago)   2m40s
```

## Run `minikube tunnel`

On completion, you'll want to run another minikube command, which you will keep
open while working on lens:

```bash
minikube tunnel
```

```console
$ minikube tunnel
[sudo] password for user:
Status:
        machine: minikube
        pid: 29444
        route: 10.96.0.0/12 -> 192.168.49.2
        minikube: Running
        services: []
    errors:
                minikube: no errors
                router: no errors
                loadbalancer emulator: no errors
```

!!! warning

    Since you will be prompted for `sudo`, it's tempting to do this:

    ```bash
    # Don't do this
    sudo minikube tunnel
    ```

    This leaves Minikube unable to find the profile for the cluster you
    created, so wait to be prompted instead:

    ```bash
    # Do this
    minikube tunnel
    ```

## Deploy OpenTofu resources

Change to the `deploy/minikube` directory when running `tofu` commands:

```bash
cd deploy/minikube
```

On first checkout, you'll need to run `tofu init`:

```bash
tofu init
```

```console
$ tofu init

Initializing the backend...

Initializing provider plugins...
- Reusing previous version of opentofu/local from the dependency lock file
- Reusing previous version of opentofu/random from the dependency lock file
- Reusing previous version of opentofu/helm from the dependency lock file
...
```

With OpenTofu initialized, you can now execute the deployment:

```bash
tofu apply
```

```console
$ tofu apply

OpenTofu used the selected providers to generate the following execution plan. Resource actions are indicated with
the following symbols:
  + create
...
...
...
Do you want to perform these actions?
  OpenTofu will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value:
```

Type `yes` to proceed with the apply:

```console
  Enter a value: yes

kubernetes_namespace.ingress: Creating...
...
...
...
helm_release.lens_postgresql: Creation complete after 56s [id=lens-postgresql]

Apply complete! Resources: 10 added, 0 changed, 0 destroyed.
...
```

!!! important

    Ensure that the [`minikube tunnel` command](#run-minikube-tunnel) is still
    running, otherwise `tofu apply` will hang when deploying the ingress
    controller Helm chart.

This deployment creates a scaffolding for your various local experiments to slot
into. It includes deployments for PostgreSQL and valkey, so that your
application has stable backing services to use. These new additions are visible
by running `kubectl get pod -A` again:

```console hl_lines="3-4 12-13"
$ kubectl get pod -A
NAMESPACE     NAME                                                              READY   STATUS    RESTARTS      AGE
ingress       ingress-nginx-ingress-controller-d46694fcd-hbqr4                  1/1     Running   0             2m23s
ingress       ingress-nginx-ingress-controller-default-backend-7f9f59b44n2j8l   1/1     Running   0             2m23s
kube-system   coredns-7db6d8ff4d-ftngl                                          1/1     Running   0             18m
kube-system   etcd-minikube                                                     1/1     Running   0             18m
kube-system   kube-apiserver-minikube                                           1/1     Running   0             18m
kube-system   kube-controller-manager-minikube                                  1/1     Running   0             18m
kube-system   kube-proxy-mh92p                                                  1/1     Running   0             18m
kube-system   kube-scheduler-minikube                                           1/1     Running   0             18m
kube-system   storage-provisioner                                               1/1     Running   1 (17m ago)   18m
lens          lens-postgresql-0                                                 1/1     Running   0             2m22s
lens          lens-valkey-master-0                                              1/1     Running   0             2m22s
```

Change back to the project root directory to continue:

```bash
cd ../..
```

## Run lens as a cluster app

At this point, you can test the lens app immediately by using the included Helm
chart.

The previous [`tofu apply`](#deploy-opentofu-resources) command creates a
`values.yaml` file in the `deploy/minikube` directory. This can be used to
deploy the lens Helm chart pre-configured for this deployment.

A skaffold configuration is provided that installs your local Helm chart to the
Minikube cluster with no additional settings needed:

```bash
skaffold dev --port-forward
```

A Makefile target is provided that does the same:

```bash
make skaffold
```

```console
$ skaffold dev --port-forward
Generating tags...
 - lens -> lens:d9474e9-dirty
Checking cache...
 - lens: Not found. Building
Starting build...
Found [minikube] context, using local docker daemon.
Building [lens]...
Target platforms: [linux/amd64]
...
...
...
Deployments stabilized in 6.107 seconds
Port forwarding service/lens-app in namespace lens, remote port 8080 -> http://127.0.0.1:8080
Listing files to watch...
 - lens
Press Ctrl+C to exit
Watching for changes...
[lens] Operations to perform:
[lens]   Apply all migrations: account, admin, auditlog, auth, contenttypes, core, db, mfa, sessions, socialaccount, users, usersessions
[lens] Running migrations:
[lens]   No migrations to apply.
[lens] [2024-10-25 19:00:47 +0000] [1] [INFO] Starting gunicorn 23.0.0
[lens] [2024-10-25 19:00:47 +0000] [1] [INFO] Listening at: http://0.0.0.0:8080 (1)
[lens] [2024-10-25 19:00:47 +0000] [1] [INFO] Using worker: sync
[lens] [2024-10-25 19:00:47 +0000] [7] [INFO] Booting worker with pid: 7
```

Once the application is stable (when it says "Watching for changes"), you can
browse to the application UI:

<http://localhost:8080/>

### Create a superuser account

The superuser account must be created to proceed any further. To do that, we
must access the `manage.py` command while `skaffold` is running.

Retrieve a list of all available management commands:

```bash
kubectl exec -it -n lens deployment/lens-app -- ./manage.py
```

The `createsuperuser` command is the first one to run. Follow the on-screen
prompts to create the first account:

!!! important

    Use a valid email, as this app will include support for transactional
    email at some point.

```bash
kubectl exec -it -n lens deployment/lens-app -- ./manage.py createsuperuser
```

```console
$ kubectl exec -it -n lens deployment/lens-app -- ./manage.py createsuperuser
Username: user
Email address: user@example.com
Password:
Password (again):
Superuser created successfully.
```

### Access the admin interface

The admin interface will be accessible at the
[`/admin/`](http://localhost:8080/admin/) URL path:

<http://localhost:8080/admin/>

Log in. When asked to verify your email, check the console logs for `skaffold`
for the "email" you've received:

```console
[lens] Content-Type: text/plain; charset="utf-8"
[lens] MIME-Version: 1.0
[lens] Content-Transfer-Encoding: 7bit
[lens] Subject: [Lens] Please Confirm Your Email Address
[lens] From: webmaster@localhost
[lens] To: user@example.com
[lens] Date: Sat, 26 Oct 2024 00:01:52 -0000
[lens] Message-ID: <172990091262.7.9414480326994878345@lens-app-98bcc98d7-472kk>
[lens]
[lens] Hello from localhost:8080!
[lens]
[lens] You're receiving this email because user breer has given your email address to register an account on localhost:8080.
[lens]
[lens] To confirm this is correct, go to http://localhost:8080/user/confirm-email/MQ:1t4UFc:Puajjg2JXLLVKOhl8cDUfUuKigOY3wBUfrh_2MHd5xI/
[lens]
[lens] Thank you for using localhost:8080!
[lens] localhost:8080
[lens] -------------------------------------------------------------------------------
```

Click the link in the email to confirm your account, and navigate to the
`/admin/` path once more:

<http://localhost:8080/admin/>

Log in again and you'll be able to access the admin interface.

## Run lens via the local development server

It's possible to run lens via Django's local development server, `manage.py
runserver.`. This allows you to take advantage of live reloading and various
debug facilities.

### Set up a virtual environment

Use the included Makefile target to set up a virtual environment:

```bash
make setup
```

Source the virtual environment:

```bash
. venv/bin/activate
```

### Create a `settings.ini` file

The previous [`tofu apply`](#deploy-opentofu-resources) command creates a
`settings.ini` file in the `deploy/minikube` directory. This file looks
something like the following:

```ini
[settings]
LENS_SECRET_KEY = XXXXXX
LENS_DATABASE_URL = postgres://lens:XXXXXX@localhost:5432/lens
LENS_CACHE_URL = redis://:XXXXXX@localhost:6379/0
LENS_BROKER_URL = redis://:XXXXXX@localhost:6379/0
LENS_SITE_ENVIRONMENT = local
```

Copy `deploy/minikube/settings.ini` to the project root directory where
`manage.py` will be executed from:

```bash
cp deploy/minikube/settings.ini settings.ini
```

A Makefile target is provided to start the local development environment. This includes:

- Forwarding ports for PostgreSQL and valkey

- Starting a generic celery worker and flower instance

- Starting Django's development server with `DEBUG` enabled

- Compiling CSS with sass

```bash
make local
```

### Enable live reloading

Adding the following to `settings.ini` causes your browser to reload whenever a
change is made to source:

```ini
LENS_LIVE_RELOAD = true
```
