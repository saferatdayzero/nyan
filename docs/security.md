# Security

## Secret key

Generate a secret key:

```bash
head /dev/urandom | tr -dc A-Za-z0-9 | head -c 64 ; echo
```

Add the `secretKey` setting:

```yaml
settings.secretKey: XXXXXX
```
