# Email

## From email

The "From:" email address must be configured to use the SMTP relay.

=== "settings.ini"

    ```ini
    [settings]
    LENS_DEFAULT_FROM_EMAIL = no-reply@lens.local
    ```

=== "values.yaml"

    ```yaml
    settings:
      email:
        defaultFromEmail: no-reply@lens.local
    ```

## Email provider

Options: **`console`** (default), **`smtp`**

By default, email messages are written to the application log. Change to `smtp`
to configure an SMTP relay.

=== "settings.ini"

    ```ini
    [settings]
    LENS_EMAIL_PROVIDER = smtp
    ```

=== "values.yaml"

    ```yaml
    settings:
      email:
        provider: smtp
    ```

## Console

No configuration required for console logging.

## SMTP

Send emails through an SMTP relay.

### Hostname

Set the hostname for the SMTP relay.

=== "settings.ini"

    ```ini
    [settings]
    LENS_EMAIL_SMTP_HOSTNAME = mail.example.com
    ```

=== "values.yaml"

    ```yaml
    settings:
      email:
        smtp:
          hostname: mail.example.com
    ```

### Password

Set the password for the SMTP relay.

=== "settings.ini"

    ```ini
    [settings]
    LENS_EMAIL_SMTP_PASSWORD = XXXXXX
    ```

=== "values.yaml"

    ```yaml
    settings:
      email:
        smtp:
          password: XXXXXX
    ```

### Port

Set the port for the SMTP relay.

=== "settings.ini"

    ```ini
    [settings]
    LENS_EMAIL_SMTP_PORT = 587
    ```

=== "values.yaml"

    ```yaml
    settings:
      email:
        smtp:
          port: 587
    ```

### SSL

Use SSL for the SMTP relay. Enabled by default.

Enable either SSL or TLS, not both.

=== "settings.ini"

    ```ini
    [settings]
    LENS_EMAIL_SMTP_SSL = false
    ```

=== "values.yaml"

    ```yaml
    settings:
      email:
        smtp:
          ssl: false
    ```

### Timeout

Time to wait in seconds for email to send before aborting.
Defaults to 180.

=== "settings.ini"

    ```ini
    [settings]
    LENS_EMAIL_SMTP_TIMEOUT = 300
    ```

=== "values.yaml"

    ```yaml
    settings:
      email:
        smtp:
          timeout: 300
    ```

### TLS

Use TLS for the SMTP relay. Disabled by default.

Enable either SSL or TLS, not both.

=== "settings.ini"

    ```ini
    [settings]
    LENS_EMAIL_SMTP_TLS = true
    ```

=== "values.yaml"

    ```yaml
    settings:
      email:
        smtp:
          tls: true
    ```

### Username

Set the username for the SMTP relay.

=== "settings.ini"

    ```ini
    [settings]
    LENS_EMAIL_SMTP_USERNAME = jdoe@example.com
    ```

=== "values.yaml"

    ```yaml
    settings:
      email:
        smtp:
          username: jdoe@example.com
    ```
