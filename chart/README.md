# dsri lens chart

Data collection and research infrastructure for the Safer@DayZero project.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| app.args | list | `[]` |  |
| app.autoscaling.enabled | bool | `false` |  |
| app.autoscaling.maxReplicas | int | `100` |  |
| app.autoscaling.minReplicas | int | `1` |  |
| app.autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| app.autoscaling.targetMemoryUtilizationPercentage | int | `80` |  |
| app.livenessProbe.httpGet.path | string | `"/health/live"` |  |
| app.livenessProbe.httpGet.port | string | `"http"` |  |
| app.networkPolicy.enabled | bool | `true` | enabled the creation of a network policy |
| app.networkPolicy.extraEgress | list | `[]` |  |
| app.networkPolicy.extraIngress | list | `[]` |  |
| app.readinessProbe.httpGet.path | string | `"/health/ready"` |  |
| app.readinessProbe.httpGet.port | string | `"http"` |  |
| app.readinessProbe.periodSeconds | int | `30` |  |
| app.readinessProbe.timeoutSeconds | int | `3` |  |
| app.replicaCount | int | `1` |  |
| app.resources | object | `{}` |  |
| app.service.port | int | `8080` |  |
| app.service.type | string | `"ClusterIP"` |  |
| celery.args | list | `[]` |  |
| celery.autoscaling.enabled | bool | `false` |  |
| celery.autoscaling.maxReplicas | int | `100` |  |
| celery.autoscaling.minReplicas | int | `1` |  |
| celery.autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| celery.autoscaling.targetMemoryUtilizationPercentage | int | `80` |  |
| celery.podAnnotations | object | `{}` |  |
| celery.podLabels | object | `{}` |  |
| celery.replicaCount | int | `1` |  |
| celery.resources | object | `{}` |  |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` |  |
| containerSecurityContext.capabilities.drop[0] | string | `"ALL"` |  |
| containerSecurityContext.privileged | bool | `false` |  |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` |  |
| containerSecurityContext.runAsGroup | int | `10001` |  |
| containerSecurityContext.runAsNonRoot | bool | `true` |  |
| containerSecurityContext.runAsUser | int | `10001` |  |
| containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` |  |
| containers | list | `[]` | Add additional containers to the deployment. |
| extraEnvVarsConfigMap | object | `{}` | Set deployment environment variables via ConfigMap. |
| extraEnvVarsSecret | object | `{}` | Set deployment environment variables via Secret. |
| focalDefaults.affinity | object | `{}` |  |
| focalDefaults.args[0] | string | `"-l"` |  |
| focalDefaults.args[1] | string | `"INFO"` |  |
| focalDefaults.autoscaling.enabled | bool | `true` |  |
| focalDefaults.command[0] | string | `"lens-focal"` |  |
| focalDefaults.command[1] | string | `"run"` |  |
| focalDefaults.nodeSelector | object | `{}` |  |
| focalDefaults.podAnnotations | object | `{}` |  |
| focalDefaults.podLabels | object | `{}` |  |
| focalDefaults.podSecurityContext.fsGroup | int | `10001` |  |
| focalDefaults.replicaCount | int | `1` |  |
| focalDefaults.resources | object | `{}` |  |
| focalDefaults.securityContext.allowPrivilegeEscalation | bool | `false` |  |
| focalDefaults.securityContext.capabilities.drop[0] | string | `"ALL"` |  |
| focalDefaults.securityContext.privileged | bool | `false` |  |
| focalDefaults.securityContext.readOnlyRootFilesystem | bool | `true` |  |
| focalDefaults.securityContext.runAsGroup | int | `10001` |  |
| focalDefaults.securityContext.runAsNonRoot | bool | `true` |  |
| focalDefaults.securityContext.runAsUser | int | `10001` |  |
| focalDefaults.tolerations | list | `[]` |  |
| focalDefaults.volumeMounts | list | `[]` |  |
| focalDefaults.volumes | list | `[]` |  |
| focals | list | `[]` | Add focals to be created here |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"registry.gitlab.com/saferatdayzero/lens"` |  |
| image.tag | string | `""` | Overrides the image tag whose default is the chart appVersion. |
| imagePullSecrets | list | `[]` |  |
| ingress.annotations | object | `{}` |  |
| ingress.className | string | `""` |  |
| ingress.enabled | bool | `true` |  |
| ingress.hosts[0].host | string | `"chart-example.local"` |  |
| ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| ingress.tls | list | `[]` |  |
| initContainers | list | `[]` | Add additional init containers to the deployment. |
| jobs.migrate.enabled | bool | `true` | Enable the database migration job. |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| podAnnotations | object | `{}` |  |
| podLabels | object | `{}` |  |
| podSecurityContext.fsGroup | int | `10001` |  |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.automount | bool | `true` | Automatically mount a ServiceAccount's API credentials? |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| settings.allowedHosts | list | `[]` | Hosts allowed to access the application. The pod itself is always allowed. |
| settings.brokerUrl | string | `""` | Celery broker URL. Lens only supports [valkey](https://valkey.io/) currently. |
| settings.cacheUrl | string | `""` | Cache URL. Lens only supports [valkey](https://valkey.io/) currently. |
| settings.databaseUrl | string | `""` | Database URL. See [dj-database-url](https://pypi.org/project/dj-database-url/#url-schema) for configuration docs. Lens only supports [PostgreSQL](https://www.postgresql.org/) currently. |
| settings.email.defaultFromEmail | string | `"no-reply@lens.local"` | Set default email from address |
| settings.email.provider | string | `"console"` | Set Email provider ex: smtp |
| settings.email.smtp.hostname | string | `""` | Set hostname for smtp provider |
| settings.email.smtp.password | string | `""` | Set password for smtp provider |
| settings.email.smtp.port | int | `587` | Set port for smtp provider |
| settings.email.smtp.ssl | bool | `true` | Enable ssl for smtp providor, True is default |
| settings.email.smtp.timeout | int | `180` | Set timeout |
| settings.email.smtp.tls | bool | `false` | Enable tls for smtp providor, False is default |
| settings.email.smtp.username | string | `""` | Set username for smtp provider |
| settings.secretKey | string | `""` | Application secret key. |
| settings.siteEnvironment | string | `""` | Text used to display deploy environment |
| settings.ssl | bool | `false` | Whether to require SSL. |
| tolerations | list | `[]` |  |
| volumeMounts | list | `[]` | Additional volumeMounts on the output Deployment definition. |
| volumes | list | `[]` | Additional volumes on the output Deployment definition. |

## License

Copyright 2024 UL Research Institutes.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
