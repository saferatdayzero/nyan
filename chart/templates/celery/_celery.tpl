{{- define "lens.celery.fullname" -}}
{{ include "lens.fullname" . }}-celery
{{- end }}

{{- define "lens.celery.labels" -}}
{{ include "lens.labels" . }}
app.kubernetes.io/component: celery
{{- end }}

{{- define "lens.celery.selectorLabels" -}}
{{ include "lens.selectorLabels" . }}
app.kubernetes.io/component: celery
{{- end }}
