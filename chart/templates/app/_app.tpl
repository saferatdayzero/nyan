{{- define "lens.app.fullname" -}}
{{ include "lens.fullname" . }}-app
{{- end }}

{{- define "lens.app.labels" -}}
{{ include "lens.labels" . }}
app.kubernetes.io/component: app
{{- end }}

{{- define "lens.app.selectorLabels" -}}
{{ include "lens.selectorLabels" . }}
app.kubernetes.io/component: app
{{- end }}

{{- define "lens.app.image" -}}
{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}
{{- end }}
