# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0
MAKEFLAGS += -j auto

.PHONY: all
all:

.PHONY: skaffold
skaffold:
	skaffold dev -n lens --port-forward

.PHONY: docs
docs:
	mkdocs serve -a localhost:8888

DOCS_DIR = docs
DOCS_OUT_DIR = $(DOCS_DIR)/out
DATA_GRAPH = $(DOCS_OUT_DIR)/data-model.png

data-model: $(DATA_GRAPH)

$(DATA_GRAPH):
	mkdir -p $(dir $(DATA_GRAPH))
	./app/manage.py graph_models resources sources findings prompts | dot -Tpng > $(DATA_GRAPH)
