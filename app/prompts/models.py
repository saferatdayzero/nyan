# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from core.casts import clean_boolean_value
from core.markdown import render_markdown
from core.validators import KeyValidator
from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from tags.models import Tag


class Category(models.Model):
    title = models.CharField(
        max_length=255,
        help_text=_("User-visible name of category."),
    )
    key = models.CharField(
        _("Key"),
        max_length=255,
        unique=True,
        validators=[KeyValidator()],
        help_text=_("Unique, human-readable identifier for the category."),
    )
    brief = models.CharField(
        blank=True,
        max_length=255,
        help_text=_("A short desciption of the category."),
    )
    description = models.TextField(
        blank=True,
        help_text=_("A longer description of the category."),
    )

    featured = models.BooleanField(
        default=True,
        help_text=_("This category should appear in top-level listings."),
    )

    visible = models.BooleanField(
        default=True,
        help_text=_("This category should be shown to users."),
    )

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    tags = models.ManyToManyField(Tag, blank=True, related_name="categories")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def get_absolute_url(self):
        return reverse("category_detail", kwargs={"category_id": self.id})

    @property
    def display_description(self):
        return render_markdown(text=self.description)


class Prompt(models.Model):
    key = models.CharField(
        _("Key"),
        max_length=255,
        unique=True,
        validators=[KeyValidator(allow_nested=True)],
        help_text=_("Unique, human-readable identifier for the prompt."),
    )

    title = models.CharField(
        max_length=255,
        help_text=_("User-visible statement or question to be considered."),
    )

    description = models.TextField(
        blank=True,
        help_text=_("A longer description of what is being evaluated."),
    )

    featured = models.BooleanField(
        default=True,
        help_text=_("This prompt should appear in top-level listings."),
    )

    visible = models.BooleanField(
        default=True, help_text=_("This prompt should be shown to users.")
    )

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    category = models.ForeignKey(
        "Category",
        related_name="prompts",
        on_delete=models.PROTECT,
        help_text=_("User-visible category to organize this prompt into."),
    )

    tags = models.ManyToManyField(Tag, blank=True, related_name="prompts")

    @property
    def display_description(self):
        return render_markdown(text=self.description)

    def get_absolute_url(self):
        return reverse("prompt_detail", kwargs={"prompt_id": self.id})

    def __str__(self):
        return f"[{self.key}]: {self.title}"

    class Meta:
        verbose_name = _("Prompt")
        verbose_name_plural = _("Prompts")


class Response(models.Model):
    prompt = models.ForeignKey(
        "Prompt",
        related_name="responses",
        on_delete=models.PROTECT,
        help_text=_("Prompt associated with this Response."),
    )

    key = models.CharField(
        max_length=255,
        help_text=_("The value returned."),
    )

    title = models.CharField(
        max_length=255,
        blank=True,
        help_text=_("Human-readable name of response."),
    )

    comparable = models.BooleanField(
        default=True,
        help_text=_(
            "This response can be directly compared with other responses for this prompt."
        ),
    )

    visible = models.BooleanField(
        default=True, help_text="This response should be shown to users."
    )

    class Cast(models.TextChoices):
        STRING = "string", _("String")
        INTEGER = "integer", _("Integer")
        BOOLEAN = "boolean", _("Boolean")

    cast = models.CharField(
        default=Cast.STRING,
        max_length=255,
        choices=Cast,
        help_text=_("Cast the value to this type."),
    )

    message = models.CharField(
        max_length=255, help_text=_("Message presented to user (plural).")
    )

    message_singular = models.CharField(
        max_length=255,
        help_text=_(
            "Message presented to user (singular/non-count). Leave blank if not applicable."
        ),
        blank=True,
    )

    description = models.TextField(
        blank=True,
        help_text=_(
            "A longer description of what is being evaluated. Can be extended Markdown content."
        ),
    )

    shelf_life = models.DurationField(
        null=True,
        blank=True,
        help_text="How long a response is considered valid. Enter DD HH:MM:SS",
    )

    supporting_responses = models.ManyToManyField(
        "Response",
        blank=True,
        related_name="supports",
        symmetrical=False,
        help_text=_("These Responses result in this Response."),
    )

    tags = models.ManyToManyField(Tag, blank=True, related_name="responses")

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def clean(self):
        if self.cast == self.Cast.BOOLEAN:
            try:
                clean_boolean_value(self.key)
            except ValueError:
                raise ValidationError({"key": _("Must be a valid boolean value.")})

    @property
    def display_description(self):
        return render_markdown(text=self.description)

    def __str__(self):
        return f"[{self.prompt.key}={self.key}]: {self.message}"

    class Meta:
        unique_together = (
            (
                "prompt",
                "key",
            ),
        )
        verbose_name = _("Response")
        verbose_name_plural = _("Responses")
