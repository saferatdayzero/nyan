# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.contrib import admin
from django.db.models import Count
from django.utils.translation import gettext_lazy as _
from reversion.admin import VersionAdmin

from .models import Category, Prompt, Response


class PromptInline(admin.TabularInline):
    model = Prompt
    extra = 0
    per_page = 5
    fields = (
        "key",
        "title",
        "visible",
    )


class ResponseInline(admin.TabularInline):
    model = Response
    extra = 0
    per_page = 5
    fields = (
        "key",
        "title",
        "cast",
        "comparable",
        "visible",
        "message",
        "message_singular",
    )

    def get_queryset(self, request):
        return super().get_queryset(request).select_related("prompt")


@admin.register(Category)
class CategoryAdmin(VersionAdmin):
    list_display = (
        "id",
        "key",
        "title",
        "featured",
        "visible",
        "total_prompts",
    )
    list_display_links = ("key",)
    readonly_fields = (
        "id",
        "created",
        "updated",
    )
    autocomplete_fields = ("tags",)
    ordering = ("title",)
    filter_horizontal = ("tags",)
    inlines = [
        PromptInline,
    ]
    search_fields = ("title",)

    fieldsets = [
        (
            None,
            {
                "fields": (
                    "key",
                    "title",
                    "brief",
                    "tags",
                    "description",
                ),
            },
        ),
        (
            _("Settings"),
            {
                "fields": (
                    "featured",
                    "visible",
                ),
            },
        ),
        (
            _("Metadata"),
            {
                "fields": (
                    "id",
                    "created",
                    "updated",
                ),
            },
        ),
    ]

    def total_prompts(self, obj):
        return obj.total_prompts

    def get_queryset(self, request):
        queryset = super().get_queryset(request).prefetch_related("prompts")
        queryset = queryset.annotate(total_prompts=Count("prompts"))
        return queryset


@admin.register(Prompt)
class PromptAdmin(VersionAdmin):
    list_display = (
        "id",
        "category__key",
        "key",
        "title",
        "featured",
        "visible",
        "total_responses",
    )
    list_display_links = (
        "category__key",
        "key",
    )
    ordering = (
        "category__key",
        "key",
    )
    readonly_fields = (
        "id",
        "created",
        "updated",
    )
    autocomplete_fields = (
        "category",
        "tags",
    )
    search_fields = (
        "key",
        "title",
    )
    filter_horizontal = ("tags",)

    fieldsets = [
        (
            None,
            {
                "fields": (
                    "category",
                    "key",
                    "title",
                    "tags",
                    "description",
                ),
            },
        ),
        (
            _("Settings"),
            {
                "fields": (
                    "featured",
                    "visible",
                ),
            },
        ),
        (
            _("Metadata"),
            {
                "fields": (
                    "id",
                    "created",
                    "updated",
                ),
            },
        ),
    ]

    inlines = [
        ResponseInline,
    ]

    def total_responses(self, obj):
        return obj.total_responses

    def get_queryset(self, request):
        queryset = super().get_queryset(request).select_related("category")
        queryset = queryset.annotate(total_responses=Count("responses"))
        return queryset


@admin.register(Response)
class ResponseAdmin(VersionAdmin):
    list_display = (
        "id",
        "category_key",
        "prompt__key",
        "key",
        "title",
        "message",
        "comparable",
        "visible",
        "total_findings",
        "total_supporting_responses",
    )
    list_display_links = (
        "category_key",
        "prompt__key",
        "key",
    )
    ordering = (
        "prompt__category__key",
        "prompt__key",
        "key",
    )
    readonly_fields = (
        "id",
        "created",
        "updated",
    )
    search_fields = (
        "prompt__key",
        "prompt__title",
        "key",
        "title",
        "message",
    )
    autocomplete_fields = (
        "prompt",
        "supporting_responses",
        "tags",
    )
    filter_horizontal = ("supporting_responses", "tags")

    fieldsets = [
        (
            None,
            {
                "fields": (
                    "prompt",
                    "key",
                    "title",
                    "message",
                    "message_singular",
                    "tags",
                    "supporting_responses",
                    "cast",
                    "shelf_life",
                ),
            },
        ),
        (
            _("Settings"),
            {
                "fields": (
                    "comparable",
                    "visible",
                ),
            },
        ),
        (
            _("Metadata"),
            {
                "fields": (
                    "id",
                    "created",
                    "updated",
                ),
            },
        ),
    ]

    @admin.display(description=_("Category key"))
    def category_key(self, obj):
        return obj.prompt.category.key

    def total_supporting_responses(self, obj):
        return obj.total_supporting_responses

    def total_findings(self, obj):
        return obj.total_findings

    def get_queryset(self, request):
        queryset = (
            super()
            .get_queryset(request)
            .select_related("prompt__category")
            .annotate(
                total_supporting_responses=Count("supporting_responses"),
                total_findings=Count("findings"),
            )
        )
        return queryset
