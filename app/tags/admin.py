# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .models import Tag


class TagAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "key",
        "title",
        "visible",
        "created",
        "updated",
    )
    list_display_links = (
        "key",
        "title",
    )
    readonly_fields = (
        "id",
        "created",
        "updated",
    )
    search_fields = (
        "key",
        "title",
    )
    ordering = ("title",)

    fieldsets = [
        (
            None,
            {
                "fields": (
                    "key",
                    "title",
                    "description",
                ),
            },
        ),
        (
            _("Settings"),
            {
                "fields": (
                    "featured",
                    "visible",
                ),
            },
        ),
        (
            _("Metadata"),
            {
                "fields": (
                    "id",
                    "created",
                    "updated",
                ),
            },
        ),
    ]


admin.site.register(Tag, TagAdmin)
