# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from core.validators import KeyValidator
from django.db import models
from django.utils.translation import gettext_lazy as _


class Tag(models.Model):

    key = models.CharField(
        _("Key"),
        max_length=255,
        unique=True,
        validators=[KeyValidator()],
        help_text=_("Unique, human-readable identifier for the tag."),
    )

    title = models.CharField(
        max_length=255,
        help_text=_("User-visible name of the tag."),
    )

    featured = models.BooleanField(
        default=True,
        help_text=_("This tag should appear in top-level listings."),
    )

    visible = models.BooleanField(
        default=True,
        help_text=_("This tag should be shown to users."),
    )

    description = models.TextField(
        blank=True,
        help_text=_("A description of the tag."),
    )

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"[{self.key}]: {self.title}"

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")
