# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.http import HttpRequest, HttpResponse


def liveness_probe(_request: HttpRequest):
    return HttpResponse(status=200)
