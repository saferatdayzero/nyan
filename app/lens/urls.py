# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from allauth.account.decorators import secure_admin_login
from debug_toolbar.toolbar import debug_toolbar_urls
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from health_check.views import MainView

from .views import liveness_probe

admin.autodiscover()
admin_site_name_suffix = (
    f" ({settings.SITE_ENVIRONMENT})" if settings.SITE_ENVIRONMENT else ""
)
admin.site.site_header = (
    f"{settings.SITE_NAME} {settings.SITE_VERSION}{admin_site_name_suffix}"
)
admin.site_title = admin_site_name_suffix + settings.SITE_NAME
admin.site.login = secure_admin_login(admin.site.login)


urlpatterns = [
    path("user/", include("allauth.urls")),
    path("user/connections/", include("allauth.socialaccount.urls")),
    path("admin/", admin.site.urls),
    path("health/live/", liveness_probe, name="health_live"),
    path("health/ready/", MainView.as_view(), name="health_ready"),
    path("", include("app.urls")),
]

if settings.LIVE_RELOAD:
    urlpatterns += [path("__reload__/", include("django_browser_reload.urls"))]

if settings.DEBUG:
    urlpatterns += debug_toolbar_urls()
