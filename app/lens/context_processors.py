# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.conf import settings


def site(request):
    return {
        "site": {
            "name": settings.SITE_NAME,
            "environment": settings.SITE_ENVIRONMENT,
            "version": settings.SITE_VERSION,
        }
    }
