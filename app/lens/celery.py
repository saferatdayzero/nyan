# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import os

from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "lens.settings")

app = Celery("lens")

app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()
