# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from .celery import app as celery_app

__all__ = ("celery_app",)
