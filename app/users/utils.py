# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import hashlib


def get_libravatar_hash(email: str) -> str:
    return hashlib.md5(email.encode("utf-8").strip().lower()).hexdigest()


def get_libravatar_url(email: str) -> str:
    return f"https://www.libravatar.org/avatar/{get_libravatar_hash(email)}?d=retro"
