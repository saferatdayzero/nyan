# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.contrib.auth import get_user_model

User = get_user_model()


def test_user_is_created(db):
    user = User.objects.create_user("test")
    assert user.username == "test"
