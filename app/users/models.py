# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.contrib.auth.models import AbstractUser

from .utils import get_libravatar_url


class CustomUser(AbstractUser):

    @property
    def avatar(self):
        return get_libravatar_url(self.email)
