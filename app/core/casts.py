# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0


def clean_boolean_value(value: str) -> bool:
    value = value.lower().strip()

    if value in (
        "true",
        "t",
        "yes",
        "y",
    ):
        return True
    elif value in (
        "false",
        "f",
        "no",
        "n",
    ):
        return False
    raise ValueError("Not a valid boolean value.")
