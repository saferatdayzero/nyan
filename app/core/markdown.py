# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from markdown_it import MarkdownIt

md = MarkdownIt("js-default")


def render_markdown(text: str) -> str:
    return md.render(text)


def render_markdown_inline(text: str) -> str:
    return md.renderInline(text)
