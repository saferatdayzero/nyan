# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import os

import jsonschema.exceptions
import yaml
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _
from jsonschema import validate

# Validator is a callable that takes a value and raises ValidationError if there's a
# problem. A validator must be serializable or it will cause problems doing migrations
# for the models.
#
# https://docs.djangoproject.com/en/5.1/ref/validators/#writing-validators
# https://docs.djangoproject.com/en/5.1/topics/migrations/#serializing-values
# https://docs.djangoproject.com/en/5.1/topics/migrations/#adding-a-deconstruct-method


@deconstructible
class SchemaValidator:
    """Validator for validating json against a jsonschema."""

    def __init__(self, schema_path):
        file_path = os.path.join(os.path.dirname(__file__), schema_path)
        with open(file_path, "r") as file:
            self.schema = yaml.safe_load(file)

    def __call__(self, value):
        try:
            validate(instance=value, schema=self.schema)
        except jsonschema.exceptions.ValidationError as e:
            raise ValidationError(_(e.message))

    def __eq__(self, other):
        return isinstance(other, SchemaValidator) and self.schema == other.schema


class KeyValidator(RegexValidator):
    """Validator for keys.

    Keys can only contain letters, numbers, and hyphens. Hyphens should only
    be used as separators. Keys cannot start or end with hyphens. Nor can
    they contain more than one contiguous hyphen.

    If `allow_nested` is True, then multiple keys separated by a single
    forward slash are allowed.
    """

    def __init__(self, allow_nested=False):
        key = r"[a-z0-9](-?[a-z0-9]+)*"
        regex = rf"^{key}$"
        message = "Only lower-case letters, numbers, and hyphens allowed."
        if allow_nested:
            regex = rf"^{key}(/{key})*$"
            message = "Only lower-case letters, numbers, hyphens, and forward slashes allowed."
        super().__init__(regex=regex, message=message)
