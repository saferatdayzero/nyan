# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.core.exceptions import ValidationError
from unittest_parametrize import ParametrizedTestCase, parametrize

from .casts import clean_boolean_value
from .validators import KeyValidator

pass_single = [
    "one",
    "one1one",
    "1224",
    "1one1",
    "a",
    "2",
    "one-1",
    "2-two",
    "1-1",
    "t-t",
    "stuff-1",
    "stuff-stuff",
    "a-b-c",
    "one-two-three",
    "one-1-two-2c",
    "w-the-f-4",
]

pass_nested = []
for first in pass_single:
    for second in pass_single:
        pass_nested.append(first + "/" + second)
        for third in pass_single:
            pass_nested.append(first + "/" + second + "/" + third)

fail_keys = [
    "-",
    "-1",
    "/what",
    "Alpha",
    "thing.1",
    "thing_2",
    "$tuff",
    "stuff-Stuff",
    "one/",
    "one-",
    "thing--double",
    "thing---triple",
    "one//two",
    "stuff#",
    "one/-",
    "-/one",
    "one1one/-1",
    "stuff#/w-the-f-4",
    "1224/Alpha",
    "one//two/one-1-two-2c",
    "1one1/thing.1",
    "thing---triple/one-two-three",
    "a/thing_2",
    "thing--double/a-b-c",
    "2/$tuff",
    "one-/stuff-stuff",
    "one-1/stuff-Stuff",
    "one//stuff-1",
    "2-two/one/",
    "stuff-Stuff/t-t",
    "1-1/one-",
    "$tuff/1-1",
    "t-t/thing--double",
    "thing_2/2-two",
    "stuff-1/thing---triple",
    "thing.1/one-1",
    "stuff-stuff/one//two",
    "Alpha/2",
    "a-b-c/stuff#",
    "-1/a",
]


class KeyValidatorTests(ParametrizedTestCase):

    #  Valid keys should pass
    @parametrize(
        "validator,key",
        [
            (validator, key)
            for validator in [KeyValidator(), KeyValidator(allow_nested=True)]
            for key in pass_single
        ],
    )
    def test_single_keys(self, validator, key):
        validator(key)

    #  Invalid keys should not pass
    @parametrize(
        "validator,key",
        [
            (validator, key)
            for validator in [KeyValidator(), KeyValidator(allow_nested=True)]
            for key in fail_keys
        ],
    )
    def test_fail_keys(self, validator, key):
        with self.assertRaises(ValidationError):
            validator(key)

    #  Nested keys should not pass if allow_nested is False
    @parametrize(
        "validator,key",
        [(validator, key) for validator in [KeyValidator()] for key in pass_nested],
    )
    def test_single_keys_only(self, validator, key):
        with self.assertRaises(ValidationError):
            validator(key)

    #  Nested keys should pass if allow_nested is True
    @parametrize(
        "validator,key",
        [
            (validator, key)
            for validator in [KeyValidator(allow_nested=True)]
            for key in pass_nested
        ],
    )
    def test_nested_keys(self, validator, key):
        validator(key)


class CleanTestCase(ParametrizedTestCase):

    @parametrize(
        "value",
        (
            ("y",),
            ("Y",),
            ("yes",),
            ("true",),
            ("TRUE",),
        ),
    )
    def test_clean_boolean_true_values(self, value):
        self.assertEqual(clean_boolean_value(value), True)

    @parametrize(
        "value",
        (
            ("FALSE",),
            ("f",),
            ("n",),
            ("no",),
            ("no",),
        ),
    )
    def test_clean_boolean_false_values(self, value):
        self.assertEqual(clean_boolean_value(value), False)

    @parametrize(
        "value,error",
        (
            ("meh", ValueError),
            ("nope", ValueError),
            ("maybe", ValueError),
            ("", ValueError),
            # this function won't verify that the passed value is a string
            (None, AttributeError),
            (False, AttributeError),
            (True, AttributeError),
            (14, AttributeError),
        ),
    )
    def test_clean_boolean_invalid_values(self, value, error):
        with self.assertRaises(error):
            clean_boolean_value(value)
