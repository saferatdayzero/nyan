# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.apps import AppConfig


class FindingsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "findings"
