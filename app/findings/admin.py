# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.contrib import admin
from django.utils import timezone

from .models import Finding


class FindingAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "resource",
        "response__prompt",
        "response",
        "source",
        "value",
        "visible",
    )
    list_display_links = (
        "resource",
        "response__prompt",
        "response",
        "source",
        "value",
    )
    readonly_fields = (
        "id",
        "created",
        "expiration",
        "contributor",
        "tags",
    )
    autocomplete_fields = (
        "resource",
        "response",
        "source",
        "contributor",
        "tags",
    )
    search_fields = (
        "resource__uri",
        "response__prompt__key",
    )
    filter_horizontal = ("tags",)

    def visible(self, obj):
        return bool(obj.response.visible)

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .select_related(
                "response__prompt__category", "resource", "source", "contributor"
            )
        )

    # https://docs.djangoproject.com/en/5.1/ref/models/fields/#durationfield
    # Arithmetic with DurationField works in most cases. However on all
    # databases other than PostgreSQL, comparing the value of a DurationField to
    # arithmetic on DateTimeField instances will not work as expected.
    def save_model(self, request, obj, form, change):
        if not obj.id:
            obj.reporter = request.user

        if "response" in form.changed_data:
            if obj.response.shelf_life is None:
                obj.expiration = None
            else:
                created = obj.created if obj.created else timezone.now()
                obj.expiration = created + obj.response.shelf_life

        super().save_model(request, obj, form, change)

    #  If expired, then don't allow editing
    def get_readonly_fields(self, request, obj=None):
        if obj and obj.expiration and obj.expiration < timezone.now():
            return [field.name for field in obj._meta.fields]
        return self.readonly_fields


admin.site.register(Finding, FindingAdmin)
