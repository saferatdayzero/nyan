# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _
from markdown_it import MarkdownIt
from tags.models import Tag

md = MarkdownIt("js-default")


def format_message(message, value):
    try:
        text = message.format(value=value)
    except KeyError:
        text = message
    return md.renderInline(text)


class Finding(models.Model):
    resource = models.ForeignKey(
        "resources.Resource",
        related_name="findings",
        on_delete=models.PROTECT,
        help_text=_("The corresponding resource."),
    )

    response = models.ForeignKey(
        "prompts.Response",
        related_name="findings",
        on_delete=models.PROTECT,
        help_text=_("The found response."),
    )

    source = models.ForeignKey(
        "sources.Source",
        related_name="findings",
        on_delete=models.PROTECT,
        help_text=_("The data source."),
    )

    value = models.CharField(
        max_length=255,
        blank=True,
        help_text=_("The string value returned."),
    )

    created = models.DateTimeField(auto_now_add=True)

    contributor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        related_name="contributed_findings",
        on_delete=models.SET_NULL,
        help_text=_("Who contributed this finding?"),
    )

    expiration = models.DateTimeField(null=True)

    description = models.TextField(
        blank=True,
        help_text=_("A longer description. Markdown is supported."),
    )

    @property
    def display_description(self):
        return render_markdown(text=self.description)

    tags = models.ManyToManyField(Tag, blank=True, related_name="findings")

    def __str__(self):
        return f"#{self.id} [{self.resource.uri}] {self.full_key}"

    @cached_property
    def message(self):
        if self.response.message_singular:
            if self.response.cast == self.response.Cast.INTEGER:
                value = int(self.value)
                if value == 1:
                    return format_message(
                        message=self.response.message_singular, value=value
                    )
        return format_message(message=self.response.message, value=self.value)

    @cached_property
    def supported_by(self):
        return self.__class__.objects.filter(
            resource=self.resource.id,
            response__in=self.response.supporting_responses.all(),
        ).select_related("response__prompt__category", "resource")

    @property
    def full_key(self):
        return f"{self.response.prompt.key}={self.response.key}"

    @property
    def display_value(self):
        if self.value:
            return self.value
        elif self.response.title:
            return self.response.title
        return self.response.key

    @property
    def source_url(self):
        if not self.source:
            return None
        if self.source.url_template:
            return self.source.url_template.format(resource=self.resource, finding=self)
        return self.source.url

    def get_absolute_url(self):
        return reverse("finding_detail", kwargs={"finding_id": self.id})

    class Meta:
        unique_together = (
            (
                "resource",
                "response",
                "source",
            ),
        )
        verbose_name = _("Finding")
        verbose_name_plural = _("Findings")
