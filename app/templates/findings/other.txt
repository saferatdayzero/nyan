
        <nav>{% for tag in finding.resource.tags.all %}<a href="#" class="font-monospace">#{{ tag.key }}</a>{% endfor %}</nav>


  <div class="col-lg-6">

    <section class="p-2 p-lg-4">
      <h3 class="mb-3 fs-5 d-flex justify-content-between">
          <span>{% trans "Response" %}</span>
          {% if user.is_staff %}<a href="{% url 'admin:prompts_response_change' finding.response.id %}" title="{% trans 'Edit response' %}"><i class="bi bi-pencil"></i></a>{% endif %}
      </h3>

      <blockquote class="my-4 blockquote bg-primary-subtle p-3">{{ finding.response.message }}</blockquote>
      {{ finding.response.display_description | safe }}
    </section>

  </div>
      <a href="{% url 'category_detail' finding.response.prompt.category.id %}">#{{ finding.response.prompt.category.title }}</a>
