# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.test import TestCase

from .models import Resource


class ResourceTests(TestCase):

    def test_resource_url(self):
        uri = "scheme://authority/path?query#fragment"
        resource = Resource.objects.create(uri=uri)
        self.assertEqual(resource.uri, uri)
        self.assertEqual(resource.scheme, "scheme")
        self.assertEqual(resource.host, "authority")
        self.assertIsNone(resource.port)
        self.assertEqual(resource.path, "/path")
        self.assertEqual(resource.query, "query")
        self.assertEqual(resource.fragment, "fragment")

    def test_resource_path_and_query(self):
        uri = "https://gitlab.com/saferatdayzero/lens-focal-2fa-directory/-/blob/main/.gitlab-ci.yml?ref_type=heads"
        resource = Resource.objects.create(uri=uri)
        self.assertEqual(resource.uri, uri)
        self.assertEqual(resource.scheme, "https")
        self.assertEqual(resource.host, "gitlab.com")
        self.assertIsNone(resource.port)
        self.assertEqual(
            resource.path,
            "/saferatdayzero/lens-focal-2fa-directory/-/blob/main/.gitlab-ci.yml",
        )
        self.assertEqual(resource.query, "ref_type=heads")
        self.assertEqual(resource.fragment, "")

    def test_resource_no_scheme(self):
        uri = "cnn.com"
        resource = Resource.objects.create(uri=uri)
        self.assertEqual(resource.uri, uri)
        self.assertEqual(resource.scheme, "")
        self.assertIsNone(resource.host)
        self.assertIsNone(resource.port)
        self.assertEqual(resource.path, "cnn.com")
        self.assertEqual(resource.query, "")
        self.assertEqual(resource.fragment, "")

    def test_resource_parameters(self):
        uri = "scheme://netloc/path;parameters?query#fragment"
        resource = Resource.objects.create(uri=uri)
        self.assertEqual(resource.uri, uri)
        self.assertEqual(resource.scheme, "scheme")
        self.assertEqual(resource.host, "netloc")
        self.assertIsNone(resource.port)
        self.assertEqual(resource.path, "/path;parameters")
        self.assertEqual(resource.query, "query")
        self.assertEqual(resource.fragment, "fragment")

    def test_resource_port(self):
        uri = "scheme://host:8080/path?query#fragment"
        resource = Resource.objects.create(uri=uri)
        self.assertEqual(resource.uri, uri)
        self.assertEqual(resource.scheme, "scheme")
        self.assertEqual(resource.host, "host")
        self.assertEqual(resource.port, 8080)
        self.assertEqual(resource.path, "/path")
        self.assertEqual(resource.query, "query")
        self.assertEqual(resource.fragment, "fragment")

    def test_resource_user(self):
        uri = "scheme://user@host:8080/path?query#fragment"
        resource = Resource.objects.create(uri=uri)
        self.assertNotEqual(resource.uri, uri)
        self.assertEqual(
            resource.uri,
            uri.replace("user@", "", 1),
        )
        self.assertEqual(resource.scheme, "scheme")
        self.assertEqual(resource.host, "host")
        self.assertEqual(resource.port, 8080)
        self.assertEqual(resource.path, "/path")
        self.assertEqual(resource.query, "query")
        self.assertEqual(resource.fragment, "fragment")

        uri = "scheme://user@host/path?query#fragment"
        resource = Resource.objects.create(uri=uri)
        self.assertNotEqual(resource.uri, uri)
        self.assertEqual(
            resource.uri,
            uri.replace("user@", "", 1),
        )
        self.assertEqual(resource.scheme, "scheme")
        self.assertEqual(resource.host, "host")
        self.assertIsNone(resource.port)
        self.assertEqual(resource.path, "/path")
        self.assertEqual(resource.query, "query")
        self.assertEqual(resource.fragment, "fragment")

    def test_resource_user_password(self):
        uri = "scheme://user:password@host:8080/path?query#fragment"  # pragma: allowlist secret
        resource = Resource.objects.create(uri=uri)
        self.assertNotEqual(resource.uri, uri)
        self.assertEqual(
            resource.uri,
            uri.replace("user:password@", "", 1),
        )
        self.assertEqual(resource.scheme, "scheme")
        self.assertEqual(resource.host, "host")
        self.assertEqual(resource.port, 8080)
        self.assertEqual(resource.path, "/path")
        self.assertEqual(resource.query, "query")
        self.assertEqual(resource.fragment, "fragment")

        uri = "scheme://user:password@host/path?query#fragment"  # pragma: allowlist secret
        resource = Resource.objects.create(uri=uri)
        self.assertNotEqual(resource.uri, uri)
        self.assertEqual(
            resource.uri,
            uri.replace("user:password@", "", 1),
        )
        self.assertEqual(resource.scheme, "scheme")
        self.assertEqual(resource.host, "host")
        self.assertIsNone(resource.port)
        self.assertEqual(resource.path, "/path")
        self.assertEqual(resource.query, "query")
        self.assertEqual(resource.fragment, "fragment")
