# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.contrib import admin
from django.db.models import Prefetch
from findings.models import Finding

from .models import Resource


class FindingInline(admin.TabularInline):
    model = Finding
    extra = 0
    per_page = 5
    autocomplete_fields = (
        "response",
        "source",
    )
    fields = (
        "response",
        "source",
        "value",
    )

    def get_queryset(self, request):
        return (
            super().get_queryset(request).select_related("response__prompt", "resource")
        )


class ResourceAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "uri",
        "title",
        "since_created",
    )
    list_display_links = (
        "uri",
        "title",
    )
    readonly_fields = (
        "id",
        "scheme",
        "host",
        "port",
        "path",
        "query",
        "fragment",
        "created",
    )
    search_fields = ("uri",)
    ordering = ("-created",)
    filter_horizontal = (
        "tags",
        "related",
    )
    autocomplete_fields = (
        "tags",
        "related",
    )

    fieldsets = [
        (
            None,
            {
                "fields": (
                    "id",
                    "uri",
                    "title",
                    "icon",
                    "tags",
                    "description",
                    "related",
                    "created",
                ),
            },
        ),
        (
            "Resource",
            {
                "fields": (
                    "scheme",
                    "host",
                    "port",
                    "path",
                    "query",
                    "fragment",
                ),
            },
        ),
    ]

    inlines = [
        FindingInline,
    ]

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .prefetch_related(
                Prefetch(
                    "findings",
                    Finding.objects.select_related("response__prompt", "resource"),
                )
            )
        )

    def save_model(self, request, obj, form, change):
        obj.parse_url()
        super().save_model(request, obj, form, change)


class ResourceInline(admin.TabularInline):
    model = Resource.tags.through
    extra = 0
    can_delete = False
    readonly_fields = ["resource"]


admin.site.register(Resource, ResourceAdmin)
