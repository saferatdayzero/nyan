# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import urllib.parse

from core.markdown import render_markdown
from django.contrib.humanize.templatetags import humanize
from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from tags.models import Tag


def validate_url(value):
    parts = urllib.parse.urlsplit(value)
    if not parts.scheme:
        raise ValidationError(_("URI must define a scheme."))


# https://en.wikipedia.org/wiki/Uniform_Resource_Identifier
# https://www.iana.org/assignments/uri-schemes/uri-schemes.xhtml
# https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers
class Resource(models.Model):
    uri = models.CharField(
        max_length=2048,
        unique=True,
        validators=[validate_url],
    )

    scheme = models.CharField(max_length=255, editable=False)
    host = models.CharField(max_length=2048, null=True, editable=False)
    port = models.PositiveIntegerField(null=True, editable=False)
    path = models.CharField(max_length=2048, blank=True, editable=False)
    query = models.CharField(max_length=2048, blank=True, editable=False)
    fragment = models.CharField(max_length=2048, blank=True, editable=False)

    created = models.DateTimeField(auto_now_add=True)
    tags = models.ManyToManyField(Tag, blank=True, related_name="resources")
    related = models.ManyToManyField("self", blank=True)
    icon = models.URLField(blank=True, null=True, max_length=2048)

    title = models.CharField(
        max_length=255,
        help_text=_("User-visible name of the resource."),
        blank=True,
        null=True,
    )

    description = models.TextField(
        blank=True,
        help_text=_(
            "A longer description of the source. Can be extended Markdown content."
        ),
    )

    def parse_url(self):
        # https://docs.python.org/3/library/urllib.parse.html
        parts = urllib.parse.urlsplit(self.uri)

        self.scheme = parts.scheme
        self.host = parts.hostname
        self.port = parts.port
        self.path = parts.path
        self.query = parts.query
        self.fragment = parts.fragment

        authority = self.host
        if self.port is not None:
            authority += f":{self.port}"

        self.uri = urllib.parse.urlunsplit(parts._replace(netloc=authority))

    def save(self, *args, **kwargs):
        self.parse_url()
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("resource_report", kwargs={"resource_id": self.id})

    @property
    def since_created(self):
        return humanize.naturaltime(self.created)

    @property
    def display_description(self):
        return render_markdown(text=self.description)

    @property
    def display_name(self):
        if self.title:
            return self.title
        if self.path:
            return self.path
        return self.uri

    def __str__(self):
        return self.uri

    class Meta:
        verbose_name = _("Resource")
        verbose_name_plural = _("Resources")
