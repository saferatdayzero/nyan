# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.contrib import admin
from reversion.admin import VersionAdmin

from .models import Source


@admin.register(Source)
class SourceAdmin(VersionAdmin):
    list_display = (
        "id",
        "key",
        "name",
        "short_name",
        "brief",
    )
    list_display_links = ("key",)
    ordering = ("key",)
    readonly_fields = (
        "id",
        "created",
        "updated",
    )
    search_fields = (
        "key",
        "name",
        "short_name",
    )
    filter_horizontal = ("tags",)
