# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from core.markdown import render_markdown
from core.validators import KeyValidator
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from tags.models import Tag


class Source(models.Model):
    key = models.CharField(
        _("Key"),
        max_length=255,
        unique=True,
        validators=[KeyValidator(allow_nested=True)],
        help_text=_("Unique, human-readable identifier for the source."),
    )

    name = models.CharField(
        max_length=255,
        help_text=_("Title of the source of the information."),
    )

    short_name = models.CharField(
        max_length=255,
        blank=True,
        help_text=_("Shorter version of the source's name, if applicable."),
    )

    @property
    def shortest_name(self):
        if self.short_name:
            return self.short_name
        return self.name

    url = models.URLField(
        blank=True, help_text=_("Home page for the information source.")
    )

    url_template = models.CharField(
        max_length=2048,
        blank=True,
        help_text=_(
            "Defines a URL template to generate a link to an external report. `resource` and `finding` can be used."
        ),
    )

    icon = models.URLField(blank=True, null=True, max_length=2048)

    brief = models.CharField(
        blank=True,
        max_length=255,
        help_text=_("A short desciption of the information source."),
    )

    description = models.TextField(
        blank=True,
        help_text=_("A longer description of the information source."),
    )

    tags = models.ManyToManyField(Tag, blank=True, related_name="sources")

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"[{self.key}]: {self.shortest_name}"

    @property
    def display_description(self):
        return render_markdown(text=self.description)

    def get_absolute_url(self):
        return reverse("source_detail", kwargs={"source_id": self.id})

    class Meta:
        verbose_name = _("Source")
        verbose_name_plural = _("Sources")
