# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import importlib

from django.conf import settings
from django.test import SimpleTestCase, TestCase
from django.urls import reverse
from unittest_parametrize import ParametrizedTestCase, parametrize

from .tasks import evaluate_conditions


class ABool:
    def __init__(self):
        self.a_false = False
        self.a_true = True
        self.a_none = None


class Values:
    def __init__(self):
        self.true = True
        self.value = "value"
        self.number = "one"
        self.letter = "c"
        self.none = None
        self.empty = ""


class ConditionTests(SimpleTestCase):

    def test_evaluate_bool_condition(self):

        resource = ABool()

        self.assertTrue(evaluate_conditions({"a_true": True}, resource))
        self.assertFalse(evaluate_conditions({"a_true": False}, resource))

        self.assertTrue(evaluate_conditions({"a_false": False}, resource))
        self.assertFalse(evaluate_conditions({"a_false": True}, resource))

        self.assertTrue(evaluate_conditions({"a_none": False}, resource))
        self.assertFalse(evaluate_conditions({"a_none": True}, resource))

    def test_evaluate_list_condition(self):

        resource = Values()

        self.assertFalse(evaluate_conditions({"value": []}, resource))
        self.assertTrue(evaluate_conditions({"number": ["one"]}, resource))
        self.assertFalse(evaluate_conditions({"number": ["two"]}, resource))
        self.assertTrue(evaluate_conditions({"letter": ["a", "b", "c", "d"]}, resource))
        self.assertFalse(
            evaluate_conditions({"letter": ["a", "b", "d", "e"]}, resource)
        )

    def test_evaluate_dict_condition(self):

        resource = Values()

        self.assertFalse(evaluate_conditions({"value": {"equals": "values"}}, resource))
        self.assertFalse(evaluate_conditions({"letter": {"in": ["a", "b"]}}, resource))
        self.assertTrue(evaluate_conditions({"value": {"equals": "value"}}, resource))
        self.assertTrue(evaluate_conditions({"letter": {"in": ["a", "c"]}}, resource))

    def test_evaluate_multiple_conditions(self):

        resource = Values()

        conditions = {
            "true": True,
            "letter": ["a", "b", "c", "d"],
            "value": {"equals": "value"},
        }
        self.assertTrue(evaluate_conditions(conditions, resource))

        conditions = {
            "true": False,
            "letter": ["a", "b", "c", "d"],
            "value": {"equals": "value"},
        }
        self.assertFalse(evaluate_conditions(conditions, resource))

        conditions = {
            "true": True,
            "letter": ["a", "b", "d"],
            "value": {"equals": "value"},
        }
        self.assertFalse(evaluate_conditions(conditions, resource))

        conditions = {
            "true": True,
            "letter": ["a", "b", "c", "d"],
            "value": {"equals": "values"},
        }
        self.assertFalse(evaluate_conditions(conditions, resource))


#  Gets urls that have a 'name' set. all_params should have values for
#  any parameters needed by a url. This does not handle positional
#  parameters so if urls are added in the future that use them, this
#  will need be changed.
def get_named_urls(urlpatterns, all_params={}, prefix=""):
    url_list = []
    for pattern in urlpatterns:
        if hasattr(pattern, "url_patterns"):
            new_prefix = prefix
            if pattern.namespace:
                new_prefix = prefix + (":" if prefix else "") + pattern.namespace
                url_list.extend(
                    get_named_urls(
                        pattern.url_patterns, all_params=all_params, prefix=new_prefix
                    )
                )
            else:
                url_list.extend(
                    get_named_urls(
                        pattern.url_patterns, all_params=all_params, prefix=new_prefix
                    )
                )
        if hasattr(pattern, "name") and pattern.name:
            name = pattern.name
        else:
            # skip urls with no name
            continue
        fullname = (prefix + ":" + name) if prefix else name
        params = {}
        regex = pattern.pattern.regex
        if regex.groups > 0:
            # the url expects parameters
            # use params set in all_params
            if regex.groups > len(regex.groupindex.keys()) or set(
                regex.groupindex.keys()
            ) - set(all_params.keys()):
                # can't test because there are positional parameters or
                # there are keyword parameters that are not in all_params
                assert False, f"unable to test {pattern}, unnamed or missing params"
            else:
                for key in set(all_params.keys()) & set(regex.groupindex.keys()):
                    params[key] = all_params[key]
        url_list.append(reverse(fullname, kwargs=params))
    return url_list


#  List urls that should require login
#  Skip urls from other apps not in this project
#  urls should redirect before parameters are evaluated, so values shouldn't matter
#  but any url requiring a parameter, should have it set in all_params
def get_login_required_urls():
    module = importlib.import_module(settings.ROOT_URLCONF)
    all_params = {
        "finding_id": 1,
        "category_id": 1,
        "prompt_id": 1,
        "resource_id": 1,
        "source_id": 1,
    }
    skip_patterns = [
        "__reload__/",
        "user/",
        "user/connections/",
        "admin/",
        "health/ready/",
        "health/live/",
    ]
    url_patterns = []
    for pattern in module.urlpatterns:
        if str(pattern.pattern) in skip_patterns:
            continue
        else:
            url_patterns.append(pattern)
    return get_named_urls(url_patterns, all_params=all_params)


class RequireLoginTests(ParametrizedTestCase, TestCase):

    # Check urls that should require login
    @parametrize("url", [(url,) for url in get_login_required_urls()])
    def test_require_login(self, url):
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302, f"{url} status not 302 {response}")
        self.assert_(response.url.find("/user/login/?") == 0)
