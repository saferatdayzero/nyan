# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import os
from contextlib import ExitStack
from io import StringIO
from mailbox import Maildir
from tempfile import TemporaryDirectory
from unittest.mock import patch

from aiosmtpd.controller import Controller
from aiosmtpd.handlers import Mailbox
from django.core.mail import send_mail
from django.test import SimpleTestCase, override_settings


def send_test_email():
    send_mail(
        "Subject here",
        "Here is the message.",
        "from@example.com",
        ["to@example.com"],
        fail_silently=False,
    )


class EmailTests(SimpleTestCase):

    @override_settings(EMAIL_BACKEND="django.core.mail.backends.console.EmailBackend")
    def test_email(self):

        with patch("sys.stdout", new=StringIO()) as fake_out:
            send_test_email()

            captured_output = fake_out.getvalue()

            self.assertIn("Here is the message.", captured_output)
            self.assertIn("From: from@example.com", captured_output)
            self.assertIn("To: to@example.com", captured_output)

    @override_settings(
        EMAIL_BACKEND="django.core.mail.backends.smtp.EmailBackend",
        EMAIL_HOST="localhost",
        EMAIL_HOST_USER="",
        EMAIL_HOST_PASSWORD="",
        EMAIL_PORT=8025,
        EMAIL_USE_SSL=False,
        EMAIL_USE_TLS=False,
    )
    def test_smtp_email(self):

        # https://aiosmtpd.aio-libs.org/en/latest/handlers.html#the-mailbox-handler
        # Clean up the temporary directory at the end
        resources = ExitStack()
        tempdir = resources.enter_context(TemporaryDirectory())

        maildir_path = os.path.join(tempdir, "maildir")
        controller = Controller(Mailbox(maildir_path))
        controller.start()
        # Arrange for the controller to be stopped at the end
        _ = resources.callback(controller.stop)

        send_test_email()

        mailbox = Maildir(maildir_path)
        messages = mailbox.values()
        self.assertEqual(len(messages), 1)
        message = messages[0].as_string()
        self.assertIn("Here is the message.", message)
        self.assertIn("From: from@example.com", message)
        self.assertIn("To: to@example.com", message)
        resources.close()
