# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0


from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.db.models import Case, Count, F, Prefetch, Q, When
from django.template.response import TemplateResponse
from django.utils import timezone
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from findings.models import Finding
from prompts.models import Category, Prompt, Response
from resources.models import Resource
from sources.models import Source
from tags.models import Tag


class HomePageView(LoginRequiredMixin, TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        most_findings = Resource.objects.annotate(
            num_findings=Count("findings"),
        ).order_by("-num_findings")[:5]

        context["popular_tags"] = (
            Tag.objects.filter(featured=True, visible=True)
            .annotate(
                num_findings=Count("resources__findings"),
            )
            .order_by("-num_findings")[:5]
        )

        if most_findings:
            context["site_report_obj"] = most_findings[0]

        context["latest_resources"] = Resource.objects.filter(
            scheme="service"
        ).order_by("-created")[:5]

        context["categories"] = Category.objects.filter(
            visible=True,
            featured=True,
        ).order_by("key")

        context["latest_findings"] = (
            Finding.objects.filter(
                resource__scheme="service",
                response__prompt__category__featured=True,
                response__prompt__category__visible=True,
                response__prompt__featured=True,
                response__prompt__visible=True,
                response__visible=True,
            )
            .order_by("-created")
            .select_related(
                "resource",
                "response__prompt__category",
            )[:5]
        )
        return context


class ResourceReportView(LoginRequiredMixin, DetailView):
    model = Resource
    slug_field = "id"
    slug_url_kwarg = "resource_id"
    context_object_name = "resource"
    template_name = "report.html"

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .prefetch_related(
                Prefetch(
                    "findings",
                    Finding.objects.filter(
                        response__prompt__category__featured=True,
                        response__prompt__category__visible=True,
                        response__prompt__featured=True,
                        response__prompt__visible=True,
                        response__visible=True,
                    )
                    .select_related("response__prompt__category", "source")
                    .exclude(expiration__lte=timezone.now(), expiration__isnull=False)
                    .annotate(
                        category_id=F("response__prompt__category__id"),
                        category_title=F("response__prompt__category__title"),
                        category_brief=F("response__prompt__category__brief"),
                        prompt_title=F("response__prompt__title"),
                    )
                    .order_by(
                        "category_title",
                        "response__prompt__key",
                    ),
                )
            )
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["latest_resources"] = Resource.objects.order_by("-created")[:5]
        return context


@login_required
def get_compare_view(request):
    resource_ids = request.GET.get("i", "")
    tag_ids = request.GET.get("t", "")

    resource_ids = [int(ii) for ii in resource_ids.split(",")] if resource_ids else []
    tag_ids = [int(tag_ids)] if tag_ids else []

    tags = Tag.objects.filter(id__in=tag_ids)

    resources = (
        Resource.objects.filter(Q(tags__in=tag_ids) | Q(id__in=resource_ids))
        .distinct("id")
        .order_by("id", "-created")
        .prefetch_related("tags")
        .all()
    )

    if not resources:
        latest_resources = Resource.objects.order_by("-created")[:2]
        ids = ",".join([str(resource.id) for resource in latest_resources])
        return TemplateResponse(
            request,
            "empty_compare.html",
            {
                "ids": ids,
            },
        )

    results = (
        Finding.objects.filter(
            resource__in=resources,
            response__prompt__category__featured=True,
            response__prompt__category__visible=True,
            response__prompt__featured=True,
            response__prompt__visible=True,
            response__visible=True,
            response__comparable=True,
        )
        .exclude(expiration__lte=timezone.now(), expiration__isnull=False)
        .select_related(
            "resource",
            "response__prompt__category",
            "source",
        )
        .annotate(
            category_title=F("response__prompt__category__title"),
            prompt_title=F("response__prompt__title"),
            resource_uri=F("resource__uri"),
        )
        .order_by(
            "category_title",
            "response__prompt__key",
            "resource__uri",
            "response__key",
        )
        .all()
    )

    categories = {}

    for result in results:
        categories.setdefault(
            result.category_title,
            {
                "category": result.response.prompt.category,
                "resources": resources,
                "prompts": {},
            },
        )

        prompts = categories[result.category_title]["prompts"]
        prompts.setdefault(
            result.response.prompt.key,
            {
                "prompt": result.response.prompt,
                "responses": {},
            },
        )

        responses = prompts[result.response.prompt.key]["responses"]
        responses.setdefault(result.resource.uri, [])
        responses[result.resource.uri].append(result)

    for category in categories.values():
        for prompt in category["prompts"].values():
            responses = prompt["responses"]

            xx = []
            for resource in resources:
                try:
                    xx.append(responses[resource.uri])
                except KeyError:
                    xx.append(None)
            prompt["response_list"] = xx

    return TemplateResponse(
        request,
        "compare.html",
        {
            "categories": categories,
            "resources": resources,
            "tags": tags,
        },
    )


class CategoryDetailView(LoginRequiredMixin, DetailView):
    model = Category
    paginate_by = 25
    slug_field = "id"
    slug_url_kwarg = "category_id"
    context_object_name = "category"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        paginator = Paginator(
            Prompt.objects.filter(category=self.get_object()).order_by("key"),
            self.paginate_by,
        )
        try:
            page = int(self.request.GET.get("page"))
        except (TypeError, ValueError):
            page = 1

        context["prompts"] = paginator.get_page(page)
        context["page_obj"] = context["prompts"]
        return context


class PromptDetailView(LoginRequiredMixin, DetailView):
    model = Prompt
    paginate_by = 25
    slug_field = "id"
    slug_url_kwarg = "prompt_id"
    context_object_name = "prompt"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        paginator = Paginator(
            Response.objects.filter(prompt=self.get_object()).order_by("key"),
            self.paginate_by,
        )
        try:
            page = int(self.request.GET.get("page"))
        except (TypeError, ValueError):
            page = 1

        context["responses"] = paginator.get_page(page)
        context["page_obj"] = context["responses"]
        return context

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related(
                "category",
            )
            .prefetch_related(
                "tags",
                "responses__tags",
            )
        )


class SourceDetailView(LoginRequiredMixin, DetailView):
    model = Source
    slug_field = "id"
    slug_url_kwarg = "source_id"
    context_object_name = "source"


class FindingDetailView(LoginRequiredMixin, DetailView):
    model = Finding
    slug_field = "id"
    slug_url_kwarg = "finding_id"
    context_object_name = "finding"

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related(
                "contributor",
                "response__prompt__category",
                "resource",
                "source",
            )
            .prefetch_related(
                "response__supporting_responses",
                "resource__tags",
            )
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        parent_string = self.request.GET.get("p", "")

        if parent_string:
            parent_ids = parent_string.split(",")
            parent_string = ",".join(parent_ids)
            preserved = Case(
                *[When(pk=pk, then=pos) for pos, pk in enumerate(parent_ids)]
            )

            try:
                parents = (
                    Finding.objects.filter(
                        id__in=parent_ids,
                        response__prompt__category__visible=True,
                        response__prompt__visible=True,
                        response__visible=True,
                    )
                    .select_related("response__prompt__category")
                    .order_by(preserved)
                    .all()
                )
                parents = [
                    {
                        "obj": parents[idx],
                        "ids": parent_ids[:idx],
                        "string": ",".join(parent_ids[:idx]),
                    }
                    for idx, _ in enumerate(parent_ids)
                ]
            except IndexError:
                parents = []
        else:
            parent_string = None
            parents = []

        context["parent_string"] = parent_string
        context["parents"] = parents

        context["supporting_findings"] = context["finding"].supported_by.filter(
            response__prompt__category__visible=True,
            response__prompt__visible=True,
            response__visible=True,
        )
        return context
