# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django import template
from markdown_it import MarkdownIt

register = template.Library()

md = MarkdownIt("js-default")


@register.filter(name="to_markdown")
def to_markdown(value):
    return md.render(value)


@register.filter(name="to_inline_markdown")
def to_inline_markdown(value):
    return md.renderInline(value)
