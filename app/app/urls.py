# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.urls import path

from .views import (
    CategoryDetailView,
    FindingDetailView,
    HomePageView,
    PromptDetailView,
    ResourceReportView,
    SourceDetailView,
    get_compare_view,
)

urlpatterns = [
    path("compare/", get_compare_view, name="resource_compare"),
    path(
        "report/<int:resource_id>", ResourceReportView.as_view(), name="resource_report"
    ),
    path(
        "finding/<int:finding_id>/", FindingDetailView.as_view(), name="finding_detail"
    ),
    path(
        "category/<int:category_id>/",
        CategoryDetailView.as_view(),
        name="category_detail",
    ),
    path(
        "prompt/<int:prompt_id>/",
        PromptDetailView.as_view(),
        name="prompt_detail",
    ),
    path(
        "source/<int:source_id>/",
        SourceDetailView.as_view(),
        name="source_detail",
    ),
    path("", HomePageView.as_view(), name="home"),
]
