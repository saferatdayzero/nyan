# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0
import logging
from datetime import timedelta

from celery import shared_task
from django.core.mail import send_mail
from django.utils import timezone
from resources.models import Resource

from lens.celery import app as celery_app


def evaluate_conditions(conditions, resource):
    logging.info("focal conditions: %s", conditions)
    if not conditions:
        return True

    results = []
    for (
        field,
        condition,
    ) in conditions.items():
        fieldvalue = getattr(resource, field)
        logging.info("resource[%s]: %s, check %s", field, fieldvalue, condition)
        if isinstance(condition, dict):
            for operator, value in condition.items():
                if operator == "equals":
                    results.append(fieldvalue == value)
                elif operator == "in":
                    results.append(any(fieldvalue == val for val in value))
                else:
                    NotImplementedError(f"operator not defined: {operator}")
        elif isinstance(condition, list):
            results.append(bool(fieldvalue in condition))
        elif isinstance(condition, bool):
            results.append(bool(fieldvalue) == condition)
        else:
            NotImplementedError(f"value not defined: {condition}")
    logging.info("focal condition results: %s", results)
    return all(results)


@celery_app.task()
def scan_resource(resource_uri: str):
    logging.info("processing resource %s", resource_uri)

    resource = Resource.objects.filter(
        uri=resource_uri, created__gt=timezone.now() - timedelta(hours=1)
    )
    if resource:
        return

    resource = Resource.objects.create(uri=resource_uri)


@shared_task()
def send_login_email_task(subject, message, from_email, recipient_list):
    send_mail(subject, message, from_email, recipient_list)
