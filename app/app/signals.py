# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from django.conf import settings
from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver

from app.tasks import send_login_email_task


@receiver(user_logged_in)
def send_login_notification(sender, request, created, **kwargs):
    subject = "Lens login notification"
    message = f"This is a notification of activity, you have logged into your account."
    current_user = request.user
    from_email = settings.DEFAULT_FROM_EMAIL
    recipient_list = [current_user.email]
    send_login_email_task.delay(subject, message, from_email, recipient_list)
